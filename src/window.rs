use std::rc::Rc;
use std::cell::RefCell;

use rand::{seq::IteratorRandom, thread_rng};

use adw;
use gtk;
use gtk::gio;
use gtk::glib;
use gtk::prelude::*;
use crate::board::{Board, History, FileType};
use crate::chessengine::SkillLevel;
use crate::config;
use shakmaty::Color;

pub struct Window {
    pub widget: gtk::ApplicationWindow,
    pub stack: gtk::Stack,
    pub toast_overlay: adw::ToastOverlay,
    pub builder: gtk::Builder,
    pub board: Rc<RefCell<Board>>,
}

impl Window {
    pub fn new() -> Self {
        let builder = gtk::Builder::from_resource("/net/danigm/gambito/window.ui");
        let widget: gtk::ApplicationWindow = builder
            .object("window")
            .expect("Failed to find the window object");

        let mainbox: gtk::Box = builder
            .object("mainbox")
            .expect("Failed to find the mainbox object");

        let stack: gtk::Stack = builder
            .object("mainstack")
            .expect("Failedd to find the mainstack object");

        let navigation: gtk::Box = builder
            .object("navigation")
            .expect("Failedd to find the navigation object");

        stack.connect_visible_child_notify(move |stack| {
            match stack.visible_child_name() {
                Some(n) if n == "game" => navigation.set_visible(true),
                _ => navigation.set_visible(false),
            };
        });

        let toast_overlay = builder.object("toast_overlay").expect("Failed to find the toast object");

        let board = Board::new();
        mainbox.append(&board.widget);
        widget.set_size_request(500, 500);
        widget.show();

        let object = Self {
            board: Rc::new(RefCell::new(board)),
            widget,
            stack,
            toast_overlay,
            builder,
        };
        object.add_actions();
        object.connect_mainmenu();

        object
    }

    fn connect_mainmenu(&self) {
        self.link_color_selection_buttons()
            .expect("Failed to link color buttons");
        self.link_level_selection_buttons()
            .expect("Failed to link level buttons");
        self.stack.set_visible_child_name("menu");
    }

    fn link_buttons(&self, buttons: Vec<gtk::ToggleButton>) {
        for b in buttons.iter() {
            b.connect_toggled(glib::clone!(@strong buttons => move |selected| {
                if selected.is_active() {
                    for b in buttons.iter().filter(|x| *x != selected) {
                        b.set_active(false);
                    }
                } else if !buttons.iter().any(|x| x.is_active()) {
                    selected.set_active(true);
                }
            }));
        }
    }

    fn link_color_selection_buttons(&self) -> Option<()> {
        let black: gtk::ToggleButton = self.builder.object("black")?;
        let random: gtk::ToggleButton = self.builder.object("random")?;
        let white: gtk::ToggleButton = self.builder.object("white")?;

        random.set_active(true);

        let buttons = vec![black, random, white];
        self.link_buttons(buttons);

        Some(())
    }

    fn link_level_selection_buttons(&self) -> Option<()> {
        let mut buttons: Vec<gtk::ToggleButton> = vec![];
        for i in 1..9 {
            buttons.push(self.builder.object(&format!("level{}", i))?);
        }
        buttons[0].set_active(true);
        self.link_buttons(buttons);
        Some(())
    }

    fn move_action(&self) -> gio::SimpleAction {
        let action = gio::SimpleAction::new("move", Some(glib::VariantTy::STRING));
        let board = self.board.clone();
        action.connect_activate(move |_action, data| {
            if let Some(value) = data {
                if let Some(text) = value.get::<String>() {
                    let _ = board.borrow_mut().do_move(text);
                }
            }
        });

        action
    }

    fn toast_action(&self) -> gio::SimpleAction {
    let action = gio::SimpleAction::new("toast", Some(glib::VariantTy::STRING));
        let w = self.toast_overlay.clone();
        action.connect_activate(move |_action, data| {
            if let Some(value) = data {
                if let Some(text) = value.get::<String>() {
                    let toast = adw::Toast::new(&text);
                    w.add_toast(&toast);
                }
            }
        });
        action
    }

    fn load_action(&self) -> gio::SimpleAction {
        let action = gio::SimpleAction::new("load", None);
        let board = self.board.clone();
        let w = self.widget.clone();
        let stack = self.stack.clone();
        action.connect_activate(move |_action, _| {
            let filechooser = gtk::FileChooserNative::new(
                None,
                Some(&w),
                gtk::FileChooserAction::Open,
                None,
                None,
            );
            // TODO: The filter name doesn't appear in the interface, something is missing here.
            let fen_filter = gtk::FileFilter::new();
            fen_filter.add_pattern("*.fen");
            fen_filter.add_pattern("*.FEN");
            fen_filter.set_name(Some(".fen"));
            filechooser.add_filter(&fen_filter);

            let pgn_filter = gtk::FileFilter::new();
            pgn_filter.add_pattern("*.pgn");
            pgn_filter.add_pattern("*.PGN");
            pgn_filter.set_name(Some(".pgn"));
            filechooser.add_filter(&pgn_filter);

            // Use this reference in the closure to keep a reference and avoid the crash
            let c = filechooser.clone();
            let board = board.clone();
            let stack = stack.clone();
            filechooser.connect_response(move |_chooser, response| {
                if response != gtk::ResponseType::Accept {
                    return;
                }

                if let Some(f) = c.file() {
                    let path = f.path().expect("Fail loading file");
                    let path = path.to_str().expect("Invalid path");
                    let file_type = match path.to_lowercase() {
                        n if n.ends_with("fen") => FileType::FEN,
                        n if n.ends_with("pgn") => FileType::PGN,
                        _ => FileType::FEN,
                    };

                    if let Err(e) = board.borrow_mut().load(path, file_type) {
                        eprintln!("Error loading FEN {e:?}");
                    } else {
                        stack.set_visible_child_name("game");
                    }
                }
            });

            filechooser.show();
        });
        action
    }

    fn rotate_board_action(&self) -> gio::SimpleAction {
        let action = gio::SimpleAction::new("rotate-board", None);
        let board = self.board.clone();
        action.connect_activate(move |_action, _data| {
            board.borrow_mut().rotate();
        });

        action
    }

    fn new_game_action(&self) -> gio::SimpleAction {
        let action = gio::SimpleAction::new("new-game", Some(glib::VariantTy::INT32));
        let board = self.board.clone();
        let builder = self.builder.clone();
        let stack = self.stack.clone();
        action.connect_activate(move |_action, data| {
            let mut time = 0;

            if let Some(value) = data {
                if let Some(n) = value.get::<i32>() {
                    time = n;
                }
            }

            let black: gtk::ToggleButton = builder.object("black")
                .expect("Failed to get black button");
            let white: gtk::ToggleButton = builder.object("white")
                .expect("Failed to get white button");

            let color = if black.is_active() {
                Color::Black
            } else if white.is_active() {
                Color::White
            } else {
                let mut rng = thread_rng();
                let v = vec![Color::Black, Color::White];
                *v.iter().choose(&mut rng).expect("Failed to get random color")
            };

            let mut level: u32 = 1;
            for i in 1..9 {
                let id = format!("level{}", i);
                let button: gtk::ToggleButton = builder.object(&id)
                    .expect(&format!("Failed to get {} button", id));
                if button.is_active() {
                    level = i;
                    break;
                }
            }

            board.borrow_mut().new_game(color,
                                        time as u32,
                                        SkillLevel::from_u32(level));
            stack.set_visible_child_name("game");
        });

        action
    }

    fn new_game_page_action(&self) -> gio::SimpleAction {
        let action = gio::SimpleAction::new("new-game-page", None);
        let stack = self.stack.clone();
        action.connect_activate(move |_action, _data| {
            stack.set_visible_child_name("menu");
        });
        action
    }

    fn about_action(&self) -> gio::SimpleAction {
        let action = gio::SimpleAction::new("about", None);
        let win = self.widget.clone();
        action.connect_activate(move |_action, _data| {
            let dialog = gtk::AboutDialog::builder()
                .logo_icon_name("net.danigm.gambito")
                .title("About Gambito")
                .website("https://gitlab.gnome.org/danigm/gambito")
                .comments("A Chess Application")
                .license_type(gtk::License::Gpl30)
                .version(config::VERSION)
                .modal(true)
                .transient_for(&win)
                .artists(vec!["Daniel García Moreno <danigm@gnome.org>".to_string()])
                .authors(vec!["Daniel García Moreno <danigm@gnome.org>".to_string()])
                .build();

            dialog.show();
        });
        action
    }

    fn analysis_action(&self) -> gio::SimpleAction {
        let action = gio::SimpleAction::new("analysis", None);
        let board = self.board.clone();
        let stack = self.stack.clone();
        action.connect_activate(move |_action, _data| {
            board.borrow_mut().analysis();
            stack.set_visible_child_name("game");
        });
        action
    }

    fn go_prev(&self) -> gio::SimpleAction {
        let action = gio::SimpleAction::new("go-prev", None);
        let board = self.board.clone();
        action.connect_activate(move |_action, _data| {
            board.borrow_mut().go(History::Prev);
        });
        action
    }

    fn go_next(&self) -> gio::SimpleAction {
        let action = gio::SimpleAction::new("go-next", None);
        let board = self.board.clone();
        action.connect_activate(move |_action, _data| {
            board.borrow_mut().go(History::Next);
        });
        action
    }

    pub fn add_actions(&self) {
        self.widget.add_action(&self.move_action());
        self.widget.add_action(&self.load_action());
        self.widget.add_action(&self.toast_action());
        self.widget.add_action(&self.rotate_board_action());
        self.widget.add_action(&self.new_game_action());
        self.widget.add_action(&self.new_game_page_action());
        self.widget.add_action(&self.about_action());
        self.widget.add_action(&self.analysis_action());

        self.widget.add_action(&self.go_prev());
        self.widget.add_action(&self.go_next());
    }
}
