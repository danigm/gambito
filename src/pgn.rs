use std::io;
use pgn_reader::{Visitor, Skip, BufferedReader, SanPlus};
use shakmaty::{Chess, Position, Move};

pub struct PGN {
    position: Chess,
    pub movements: Vec<Move>,
    pub positions: Vec<Chess>,
}

impl PGN {
    pub fn from_bytes(bytes: &[u8]) -> io::Result<Vec<Chess>> {
        let mut reader = BufferedReader::new_cursor(bytes);
        let mut pgn = PGN {
            movements: vec![],
            positions: vec![],
            position: Chess::default(),
        };
        let error = std::io::Error::new(std::io::ErrorKind::Other, "oh no! Loading PGN");
        let moves = reader.read_game(&mut pgn)?.ok_or(error)?;
        Ok(moves)
    }
}

impl Visitor for PGN {
    type Result = Vec<Chess>;

    fn begin_game(&mut self) {
        self.position = Chess::default();
        self.movements = vec![];
        self.positions = vec![self.position.clone()];
    }

    fn san(&mut self, san_plus: SanPlus) {
        if let Ok(m) = san_plus.san.to_move(&self.position) {
            let p = self.position.clone();
            if let Ok(chess) = p.play(&m) {
                self.positions.push(chess.clone());
                self.position = chess;
            }
            self.movements.push(m);
        };
    }

    fn begin_variation(&mut self) -> Skip {
        Skip(true) // stay in the mainline
    }

    fn end_game(&mut self) -> Self::Result {
        self.positions.iter().cloned().collect()
    }
}
