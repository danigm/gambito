use shakmaty::{Color, Piece, Role};

pub trait GambitoPiece {
    fn get_class(&self) -> String {
        format!("piece-{}", self.get_text())
    }

    fn get_text(&self) -> String;
    fn get_emoji(&self) -> &str;
}

impl GambitoPiece for Piece {
    fn get_text(&self) -> String {
        let t = self.role.upper_char();
        let c = self.color.char();
        format!("{c}{t}")
    }

    fn get_emoji(&self) -> &str {
        match self.color {
            Color::White => {
                match self.role {
                    Role::Rook => "♖",
                    Role::Knight => "♘",
                    Role::Bishop => "♗",
                    Role::Queen => "♕",
                    Role::King => "♔",
                    Role::Pawn => "♙",
                }
            },
            Color::Black => {
                match self.role {
                    Role::Rook => "♜",
                    Role::Knight => "♞",
                    Role::Bishop => "♝",
                    Role::Queen => "♛",
                    Role::King => "♚",
                    Role::Pawn => "♟️",
                }
            },
        }
    }
}
