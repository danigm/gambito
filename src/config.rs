#![allow(unused_variables)]
#![allow(dead_code)]
pub static PKGDATADIR: &str = "/app/share/gambito";
pub static VERSION: &str = "0.1.0";
pub static LOCALEDIR: &str = "/app/share/locale";
pub static ENGINE: &str = "/app/bin/stockfish";
