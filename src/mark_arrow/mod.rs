mod imp;

use gtk::glib;
use gtk::gdk::RGBA;
use gtk::prelude::DrawingAreaExtManual;
use gtk::subclass::prelude::ObjectSubclassIsExt;

use gtk::prelude::WidgetExt;

use shakmaty::{ File, Rank };

glib::wrapper! {
    pub struct MarkArrow(ObjectSubclass<imp::MarkArrow>)
        @extends gtk::Widget, gtk::DrawingArea,
        @implements gtk::Accessible;
}

impl Default for MarkArrow {
    fn default() -> Self {
        Self::new(File::A, Rank::First, None)
    }
}

impl MarkArrow {
    pub fn new(file: File, rank: Rank, color: Option<RGBA>) -> Self {
        let object: Self = glib::Object::new(&[]).expect("Failed to create Arrow");
        object.set_can_target(false);
        let priv_ = object.imp();
        *priv_.src.borrow_mut() = Some((file, rank));
        *priv_.dst.borrow_mut() = None;
        *priv_.color.borrow_mut() = color;

        let f: i8 = file.into();
        let r: i8 = rank.into();
        let c = color.unwrap_or(Self::colors().0);

        let s = object.clone();
        object.set_draw_func(move |_da, ctx, width, _height| {
            let size = width as f64 / 8.0;
            let arrow_size = size / 3.0;
            let arrow_rectangle = size / 4.0;
            let arrow_rectangle2 = arrow_rectangle / 2.0;

            let priv_ = s.imp();
            let (dst_file, dst_rank) = priv_.dst.borrow().unwrap_or((file, rank));
            let f1: i8 = dst_file.into();
            let r1: i8 = dst_rank.into();
            // d = sqrt((f1 - f)^2 + (r1 - r)^2)
            let p1 = (f1 - f).pow(2);
            let p2 = (r1 - r).pow(2);
            let p3: f64 = (p1 + p2).into();
            let length: f64 = p3.sqrt();

            // angle = atan2(dy, dx)
            let angle = ((r - r1) as f64).atan2((f1 - f) as f64);

            // Arrow rectangle
            let x = size * (f as f64) + size / 2.0 - arrow_rectangle / 2.0;
            let mut y = size * (r as f64) + size / 2.0 - arrow_rectangle / 2.0;
            y = width as f64 - y;

            ctx.set_source_rgba(c.red().into(), c.green().into(), c.blue().into(), c.alpha().into());
            ctx.translate(x + arrow_rectangle2, y - arrow_rectangle2);
            ctx.rotate(angle);

            // Arrow rectangle
            ctx.rectangle(
                -arrow_rectangle2,
                -arrow_rectangle2,
                length * size - arrow_rectangle2,
                arrow_rectangle
            );
            ctx.fill().unwrap();

            // Arrow triangle
            let posx = length * size - arrow_rectangle;
            let posy = -arrow_size;

            ctx.move_to(posx, posy);
            ctx.line_to(posx + arrow_size, 0.0);
            ctx.line_to(posx, arrow_size);
            ctx.close_path();

            ctx.fill().unwrap();
        });

        object
    }

    pub fn is_eq(&self, other: &MarkArrow) -> bool {
        self.src() == other.src() && self.dst() == other.dst()
    }

    pub fn set_dst(&self, file: File, rank: Rank) {
        *self.imp().dst.borrow_mut() = Some((file, rank));
        self.queue_draw();
    }

    pub fn src(&self) -> Option<(File, Rank)> {
        self.imp().src.borrow().clone()
    }

    pub fn dst(&self) -> Option<(File, Rank)> {
        self.imp().dst.borrow().clone()
    }

    pub fn colors() -> (RGBA, RGBA, RGBA) {
        let alpha = 0.4;
        (
            RGBA::new(1.0, 0.0, 0.0, alpha),
            RGBA::new(0.0, 1.0, 0.0, alpha),
            RGBA::new(0.0, 0.0, 1.0, alpha),
        )
    }
}
