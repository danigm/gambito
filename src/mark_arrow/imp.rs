use gtk::glib;
use gtk::gdk;
use gtk::subclass::prelude::*;
use std::cell::RefCell;
use shakmaty::{File, Rank};

#[derive(Debug, Default)]
pub struct MarkArrow {
    pub src: RefCell<Option<(File, Rank)>>,
    pub dst: RefCell<Option<(File, Rank)>>,
    pub color: RefCell<Option<gdk::RGBA>>,
}

#[glib::object_subclass]
impl ObjectSubclass for MarkArrow {
    const NAME: &'static str = "MarkArrow";
    type Type = super::MarkArrow;
    type ParentType = gtk::DrawingArea;

    fn class_init(klass: &mut Self::Class) {
        klass.set_css_name("mark");
    }
}

impl ObjectImpl for MarkArrow {}
impl WidgetImpl for MarkArrow {}
impl DrawingAreaImpl for MarkArrow {}
