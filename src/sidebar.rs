/// Sidebar panel with clocks, movements and board information

use std::time::{Duration, Instant};
use gtk::prelude::*;
use gtk::glib;
use shakmaty::{Color, Position, Move, CastlingMode, Piece};
use crate::piece::GambitoPiece;

const DEFAULT_TIME: u64 = 300;

pub enum SidebarState {
    Running,
    Timeout(Color),
}

#[derive(Clone)]
#[derive(Debug)]
struct Clock {
    pub widget: gtk::Label,
    #[allow(dead_code)]
    pub color: Color,
    pub duration: Duration,
    pub initial_duration: Duration,
    pub instant: Option<Instant>,
}

impl Clock {
    pub fn new(time: u64, color: Color) -> Self {
        let duration = Duration::from_secs(time);
        let initial_duration = duration.clone();
        let widget = gtk::Label::new(Some("00:00"));
        widget.add_css_class("clock");

        Self {
            widget,
            color,
            duration,
            initial_duration,
            instant: None,
        }
    }

    pub fn reset(&mut self) {
        self.instant = None;
        self.duration = self.initial_duration.clone();
        self.update();
    }

    pub fn start(&mut self) {
        self.instant = Some(Instant::now());
        self.update();
    }

    pub fn stop(&mut self) {
        self.update();
        self.instant = None;
    }

    pub fn update(&mut self) -> bool {
        if let Some(ref mut instant) = self.instant {
            let sub = self.duration.checked_sub(instant.elapsed());
            match sub {
                Some(newd) => {
                    self.duration = newd;
                    *instant = Instant::now();
                    self.set_text(&self.duration_text());
                    true
                },
                None => {
                    self.set_text("00:00!");
                    false
                }
            }
        } else {
            self.set_text(&self.duration_text());
            true
        }
    }

    pub fn set_text(&self, text: &str) {
        self.widget.set_text(text);
    }

    pub fn duration_text(&self) -> String {
        let secs = self.duration.as_secs();
        let minutes = secs / 60;
        let seconds = secs % 60;
        format!("{minutes:02}:{seconds:02}")
    }
}

#[derive(Clone)]
#[derive(Debug)]
pub struct Sidebar {
    pub widget: gtk::Box,
    pub rotated: bool,
    white_clock: Clock,
    black_clock: Clock,
    movements: gtk::TreeView,
    movements_iter: Option<gtk::TreeIter>,
    notime: bool,
}

impl Sidebar {
    pub fn new() -> Self {
        let widget = gtk::Box::new(gtk::Orientation::Vertical, 0);
        widget.set_height_request(400);
        widget.set_width_request(400);

        let mut white_clock = Clock::new(DEFAULT_TIME, Color::White);
        let mut black_clock = Clock::new(DEFAULT_TIME, Color::Black);
        white_clock.update();
        black_clock.update();

        let model = gtk::TreeStore::new(&[glib::types::Type::STRING, glib::types::Type::STRING]);
        let movements = gtk::TreeView::with_model(&model);
        movements.set_vexpand(true);

        let column = gtk::TreeViewColumn::new();
        column.set_title("A");
        column.set_expand(true);
        let render = gtk::CellRendererText::new();
        column.pack_start(&render, true);
        column.add_attribute(&render, "text", 0);
        movements.insert_column(&column, 0);

        let column = gtk::TreeViewColumn::new();
        column.set_title("B");
        column.set_expand(true);
        let render = gtk::CellRendererText::new();
        column.pack_start(&render, true);
        column.add_attribute(&render, "text", 1);
        movements.insert_column(&column, 1);

        let scroll = gtk::ScrolledWindow::new();
        scroll.set_child(Some(&movements));

        let adjustment = scroll.vadjustment();
        adjustment.connect_changed(|adj| {
            adj.set_value(adj.upper());
        });

        widget.append(&black_clock.widget);
        widget.append(&scroll);
        widget.append(&white_clock.widget);

        widget.show();

        Self {
            widget,
            white_clock,
            black_clock,
            movements,
            movements_iter: None,
            rotated: false,
            notime: false,
        }
    }

    pub fn turn<T>(&mut self, position: &T, movement: &Move) where T: Position {
        if !self.notime {
            match position.turn() {
                Color::White => {
                    self.black_clock.stop();
                    self.white_clock.start();
                },
                Color::Black => {
                    self.black_clock.start();
                    self.white_clock.stop();
                },
            };
        }

        self.add_movement(!position.turn(), movement);

    }

    pub fn update(&mut self) -> SidebarState {
        if self.notime {
            self.white_clock.set_text("∞");
            self.black_clock.set_text("∞");
            return SidebarState::Running;
        }

        if !self.white_clock.update() {
            return SidebarState::Timeout(Color::White);
        }
        if !self.black_clock.update() {
            return SidebarState::Timeout(Color::Black);
        }
        SidebarState::Running
    }

    pub fn add_movement(&mut self, turn: Color, movement: &Move) {
        if let Some(ref model) = self.movements.model() {
            let treestore = model.downcast_ref::<gtk::TreeStore>().unwrap();
            let column = match turn {
                Color::White => 0,
                Color::Black => 1,
            };

            let uci = movement.to_uci(CastlingMode::Standard);
            let piece = Piece { color: turn, role: movement.role() };
            let image = piece.get_emoji();
            let text = format!("{image}{uci}");

            if turn == Color::White {
                self.movements_iter = Some(treestore.append(None));
            }
            if let Some(ref iter) = self.movements_iter {
                treestore.set_value(iter, column, &glib::value::Value::from(&text));
            }
        }
    }

    pub fn clear(&mut self) {
        if let Some(ref model) = self.movements.model() {
            let treestore = model.downcast_ref::<gtk::TreeStore>().unwrap();
            treestore.clear();
        }

        self.white_clock.reset();
        self.black_clock.reset();
    }

    pub fn set_time(&mut self, time: Option<u64>) {
        match time {
            Some(t) => self.set_clock_time(t),
            None => self.notime = true,
        };
    }

    pub fn set_clock_time(&mut self, time: u64) {
        self.notime = false;
        let duration = Duration::from_secs(time);
        self.white_clock.initial_duration = duration.clone();
        self.black_clock.initial_duration = duration;
        self.white_clock.reset();
        self.black_clock.reset();
        self.white_clock.update();
        self.black_clock.update();
    }

    pub fn rotate(&mut self) {
        self.rotated = !self.rotated;

        let (top, bottom) = match self.rotated {
            false => (&self.black_clock.widget, &self.white_clock.widget),
            true => (&self.white_clock.widget, &self.black_clock.widget),
        };

        self.widget.remove(top);
        self.widget.remove(bottom);

        self.widget.prepend(top);
        self.widget.append(bottom);
    }

    pub fn stopclocks(&mut self) {
        self.black_clock.stop();
        self.white_clock.stop();
    }
}
