use uci::{Engine, EngineError};
use shakmaty::{fen::Fen, Move, Chess, EnPassantMode};
use shakmaty::uci::Uci;

use crate::config;
use std::thread;
use std::time::Duration;

use std::sync::{Arc, Mutex};

use futures::prelude::*;
use futures;
use futures::channel::oneshot::{channel, Canceled};

pub struct ChessEngine {
    engine: Arc<Mutex<Engine>>,
    position: Chess,
}

impl ChessEngine {
    pub fn new() -> Self {
        let engine = Engine::new(config::ENGINE).expect("Cannot start the engine");
        let engine = Arc::new(Mutex::new(engine));
        let position = Chess::default();
        Self { engine, position }
    }

    pub fn set_level(&mut self, level: SkillLevel) -> Result<(), EngineError> {
        let mut engine = Engine::new(config::ENGINE).expect("Cannot start the engine");
        engine = engine.movetime(level.time().as_millis() as u32);
        engine = engine.depth(Some(level.depth().into()));

        engine.set_option("UCI_LimitStrength", "true")?;
        engine.set_option("UCI_AnalyseMode", "false")?;
        engine.set_option("Skill Level", &format!("{}", level.skill_level()))?;
        engine.set_option("Threads", "8")?;
        engine.set_option("Ponder", "false")?;

        self.engine = Arc::new(Mutex::new(engine));
        Ok(())
    }

    pub fn calculate_move(&self) -> impl Future<Output = Result<Move, Canceled>> {
        let engine = self.engine.clone();
        let position = self.position.clone();

        let (sender, receiver) = channel();
        thread::spawn(move || -> Option<()> {
            thread::sleep(Duration::new(1, 0));
            let engine = engine.lock().ok()?;
            let m = engine.bestmove().ok()?;
            let uci = Uci::from_ascii(m.as_bytes()).ok()?;
            let m = uci.to_move(&position).ok()?;
            sender.send(m).ok()?;

            None
        });

        receiver
    }

    pub fn evaluate(&self) -> impl Future<Output = Result<i32, Canceled>> {
        let engine = self.engine.clone();

        let (sender, receiver) = channel();
        thread::spawn(move || -> Option<()> {
            let engine = engine.lock().ok()?;
            let e = engine.evaluation().ok()?;
            sender.send(e).ok()?;

            None
        });

        receiver
    }

    pub fn update(&mut self, position: Chess) {
        self.position = position;
        let f = Fen::from_position(self.position.clone(), EnPassantMode::Legal);
        if let Ok(engine) = self.engine.lock() {
            if let Err(e) = engine.set_position(&f.to_string()) {
                panic!("Error in engine: {}", e);
            }
        }
    }
}


// copied from https://github.com/lichess-org/fishnet/blob/master/src/api.rs#L222
#[derive(Debug, Copy, Clone)]
#[repr(u32)]
pub enum SkillLevel {
    One = 1,
    Two = 2,
    Three = 3,
    Four = 4,
    Five = 5,
    Six = 6,
    Seven = 7,
    Eight = 8,
}

impl SkillLevel {
    pub fn from_u32(n: u32) -> Self {
        match n {
            1 => SkillLevel::One,
            2 => SkillLevel::Two,
            3 => SkillLevel::Three,
            4 => SkillLevel::Four,
            5 => SkillLevel::Five,
            6 => SkillLevel::Six,
            7 => SkillLevel::Seven,
            8 => SkillLevel::Eight,
            _ => SkillLevel::One,
        }
    }

    pub fn time(self) -> Duration {
        use SkillLevel::*;
        Duration::from_millis(match self {
            One => 50,
            Two => 100,
            Three => 150,
            Four => 200,
            Five => 300,
            Six => 400,
            Seven => 500,
            Eight => 1000,
        })
    }

    pub fn skill_level(self) -> i32 {
        use SkillLevel::*;
        match self {
            One => 0,
            Two => 1,
            Three => 2,
            Four => 5,
            Five => 10,
            Six => 12,
            Seven => 16,
            Eight => 20,
        }
    }

    pub fn depth(self) -> u8 {
        use SkillLevel::*;
        match self {
            One | Two | Three | Four | Five => 1,
            Six => 8,
            Seven => 13,
            Eight => 22,
        }
    }
}
