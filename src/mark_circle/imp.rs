use gtk::glib;
use gtk::gdk;
use gtk::subclass::prelude::*;
use std::cell::RefCell;
use shakmaty::{File, Rank};

#[derive(Debug, Default)]
pub struct MarkCircle {
    pub file: RefCell<Option<File>>,
    pub rank: RefCell<Option<Rank>>,
    pub color: RefCell<Option<gdk::RGBA>>,
}

#[glib::object_subclass]
impl ObjectSubclass for MarkCircle {
    const NAME: &'static str = "MarkCircle";
    type Type = super::MarkCircle;
    type ParentType = gtk::DrawingArea;

    fn class_init(klass: &mut Self::Class) {
        klass.set_css_name("mark");
    }
}

impl ObjectImpl for MarkCircle {}
impl WidgetImpl for MarkCircle {}
impl DrawingAreaImpl for MarkCircle {}
