mod imp;

use gtk::glib;
use gtk::gdk::RGBA;
use gtk::prelude::DrawingAreaExtManual;
use gtk::subclass::prelude::ObjectSubclassIsExt;

use shakmaty::{ File, Rank };

glib::wrapper! {
    pub struct MarkCircle(ObjectSubclass<imp::MarkCircle>)
        @extends gtk::Widget, gtk::DrawingArea,
        @implements gtk::Accessible;
}

impl Default for MarkCircle {
    fn default() -> Self {
        Self::new(File::A, Rank::First, None)
    }
}

impl MarkCircle {
    pub fn new(file: File, rank: Rank, color: Option<RGBA>) -> Self {
        let object: Self = glib::Object::new(&[]).expect("Failed to create Circle");
        let priv_ = object.imp();
        *priv_.file.borrow_mut() = Some(file);
        *priv_.rank.borrow_mut() = Some(rank);
        *priv_.color.borrow_mut() = color;

        let f: u8 = file.into();
        let r: u8 = rank.into();
        let c = color.unwrap_or(Self::colors().0);

        object.set_draw_func(move |_da, ctx, width, _height| {
            let size = width as f64 / 8.0;
            let radius = size / 2.0;
            let posx = size * f as f64 + radius;
            let mut posy = size * r as f64 + radius;
            posy = width as f64 - posy;

            ctx.arc(posx, posy, radius,
                    0.0, 2.0 * std::f64::consts::PI);

            ctx.set_source_rgba(c.red().into(), c.green().into(), c.blue().into(), c.alpha().into());
            let _ = ctx.fill();
        });

        object
    }

    pub fn file(&self) -> File {
        match *self.imp().file.borrow() {
            Some(f) => f,
            None => File::A,
        }
    }

    pub fn rank(&self) -> Rank {
        match *self.imp().rank.borrow() {
            Some(r) => r,
            None => Rank::First,
        }
    }

    pub fn colors() -> (RGBA, RGBA, RGBA) {
        let alpha = 0.4;
        (
            RGBA::new(1.0, 0.0, 0.0, alpha),
            RGBA::new(0.0, 1.0, 0.0, alpha),
            RGBA::new(0.0, 0.0, 1.0, alpha),
        )
    }
}
