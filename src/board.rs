use std::fs::File;
use std::io::prelude::*;

use std::rc::Rc;
use std::cell::RefCell;
use std::time::Duration;
use std::convert::TryInto;

use std::fmt;
use gtk;
use gtk::gdk;
use gtk::glib;
use gtk::prelude::*;
use shakmaty;
use shakmaty::{Color, Piece, Chess, Position, Square, Move, Role};
use shakmaty::{fen::Fen, CastlingMode};
use crate::cell::Cell;
use crate::sidebar::{Sidebar, SidebarState};
use crate::chessengine::{ChessEngine, SkillLevel};
use crate::mark_circle::MarkCircle;
use crate::mark_arrow::MarkArrow;
use crate::pgn::PGN;

pub enum FileType {
    FEN,
    PGN,
}

struct BoardPrivate {
    pub chess: Chess,
    pub movements: Vec<Chess>,
    pub current_pos: usize,
    pub board: [[Cell; 8]; 8],
    pub sidebar: Sidebar,
    pub engine: ChessEngine,
    pub canmove: bool,
    pub timeout_id: Option<glib::source::SourceId>,
    pub marks: Vec<MarkCircle>,
    pub arrows: Vec<MarkArrow>,
    pub suggestions: Vec<MarkArrow>,
}

#[derive(Clone)]
pub struct Board {
    pub overlay: gtk::Overlay,
    pub grid: gtk::Grid,
    pub widget: gtk::Box,
    pub board_widget: gtk::AspectFrame,
    pub analysis_bar: gtk::LevelBar,
    pub rotated: bool,
    pub computer: Option<Color>,
    pub show_best_move: bool,
    private: Rc<RefCell<BoardPrivate>>,
}

#[derive(Debug)]
pub enum History {
    Next,
    Prev,
}

impl Board {
    pub fn new() -> Self {
        let chess = Chess::default();
        let grid = gtk::Grid::new();
        let widget = gtk::Box::new(gtk::Orientation::Horizontal, 0);
        let overlay = gtk::Overlay::new();
        let board_widget = gtk::AspectFrame::new(0.5, 0.0, 1.0, false);
        board_widget.set_height_request(500);
        board_widget.set_width_request(500);

        let board = [
            Self::create_row(0),
            Self::create_row(1),
            Self::create_row(2),
            Self::create_row(3),
            Self::create_row(4),
            Self::create_row(5),
            Self::create_row(6),
            Self::create_row(7),
        ];

        let sidebar = Sidebar::new();
        let analysis_bar = gtk::LevelBar::builder()
            .min_value(0f64)
            .max_value(100f64)
            .value(50f64)
            .orientation(gtk::Orientation::Vertical)
            .visible(false)
            .css_classes(vec![String::from("analysis-bar")])
            .build();

        overlay.set_child(Some(&grid));
        board_widget.set_child(Some(&overlay));
        // Widgets to the root box
        widget.append(&board_widget);
        widget.append(&analysis_bar);
        widget.append(&sidebar.widget);

        let mut engine = ChessEngine::new();
        engine.set_level(SkillLevel::One).expect("Error setting engine skill level");

        let private = BoardPrivate {
            board,
            sidebar,
            chess,
            engine,
            timeout_id: None,
            canmove: true,
            marks: vec![],
            arrows: vec![],
            suggestions: vec![],
            movements: vec![],
            current_pos: 0,
        };

        let private = Rc::new(RefCell::new(private));

        let object = Self {
            widget,
            board_widget,
            analysis_bar,
            grid,
            overlay,
            private,
            rotated: false,
            computer: None,
            show_best_move: false,
        };

        object.initialize_cells();
        object.reload_board_cells();
        object.add_cell_drop_target();
        object.update_sidebar();
        object.bind_right_click();
        object.bind_drag();

        object
    }

    fn main_window(&self) -> Option<gtk::Window> {
        let app = gtk::gio::Application::default()?;
        let app = app.downcast::<gtk::Application>().ok()?;
        app.active_window()
    }

    fn bind_right_click(&self) {
        let right_click = gtk::GestureClick::new();
        self.overlay.add_controller(&right_click);

        right_click.set_button(0);
        let b = self.clone();
        right_click.connect_released(move |gesture, _n, x, y| {
            let modifiers = gesture.current_event_state();
            match gesture.current_button() {
                1 => { b.remove_marks(); },
                3 => { b.add_mark(x, y, modifiers); },
                _ => {},
            };
        });
    }

    fn bind_drag(&self) {
        let drag = gtk::GestureDrag::new();
        self.overlay.add_controller(&drag);
        drag.set_button(3);
        let b = self.clone();
        drag.connect_drag_begin(move |gesture, x, y| {
            let modifiers = gesture.current_event_state();
            b.add_arrow(x, y, modifiers);
        });

        let b = self.clone();
        drag.connect_drag_update(move |gesture, dx, dy| {
            if let Some((sx, sy)) = gesture.start_point() {
                b.update_arrow(sx + dx, sy + dy);
            }
        });

        let b = self.clone();
        drag.connect_drag_end(move |gesture, dx, dy| {
            if let Some((sx, sy)) = gesture.start_point() {
                if b.update_arrow(sx + dx, sy + dy) {
                    gesture.set_state(gtk::EventSequenceState::Claimed);
                }
            }

            b.remove_hidden_arrows();
        });
    }

    fn remove_marks(&self) {
        let mut p = self.private.borrow_mut();
        for mark in p.marks.iter() {
            self.overlay.remove_overlay(mark);
        };
        p.marks = vec![];
        for mark in p.arrows.iter() {
            self.overlay.remove_overlay(mark);
        };
        p.arrows = vec![];
    }

    fn remove_hidden_arrows(&self) {
        let mut priv_ = self.private.borrow_mut();
        let mut to_remove: Vec<(usize, MarkArrow)> = vec![];
        let length = priv_.arrows.len();
        let other_arrows = priv_.arrows.iter().take(length - 1);

        if let Some(current) = priv_.arrows.last() {
            if !current.is_visible() {
                to_remove.push((length - 1, current.clone()));
            } else {
                if let Some((i, other)) = other_arrows.enumerate().find(|(_i, a)| { a.is_eq(current) }) {
                    to_remove.push((length - 1, current.clone()));
                    to_remove.push((i, other.clone()));
                }
            }
        }

        for (index, arrow) in to_remove {
            self.overlay.remove_overlay(&arrow);
            priv_.arrows.remove(index);
        }
    }

    fn remove_suggestions(&self) {
        let mut p = self.private.borrow_mut();
        for mark in p.suggestions.iter() {
            self.overlay.remove_overlay(mark);
        };
        p.suggestions = vec![];
    }

    fn show_move_arrow(&self, movement: Move) {
        self.remove_suggestions();
        let color = None;
        if let Some(f) = movement.from() {
            let t = movement.to();
            let new_mark = MarkArrow::new(f.file(), f.rank(), color);
            new_mark.set_dst(t.file(), t.rank());
            self.overlay.add_overlay(&new_mark);
            {
                let mut p = self.private.borrow_mut();
                p.suggestions.push(new_mark);
            }
        }
    }

    fn update_arrow(&self, x: f64, y: f64) -> bool {
        let priv_ = self.private.borrow();
        if let Some(current_arrow) = priv_.arrows.last() {
            let board = self.grid.width() as f64;
            let posx = (x * 8.0 / board) as u8;
            let posy = ((board - y) * 8.0 / board) as u8;

            let file: shakmaty::File = posx.try_into().unwrap_or(shakmaty::File::A);
            let rank: shakmaty::Rank = posy.try_into().unwrap_or(shakmaty::Rank::First);

            current_arrow.set_dst(file, rank);
            let dst = current_arrow.dst();
            let src = current_arrow.src();
            current_arrow.set_visible(src != dst);

            return src != dst;
        }

        false
    }

    fn add_arrow(&self, x: f64, y: f64, modifiers: gtk::gdk::ModifierType) {
        let mut p = self.private.borrow_mut();

        let board = self.grid.width() as f64;
        let posx = (x * 8.0 / board) as u8;
        let posy = ((board - y) * 8.0 / board) as u8;

        let file: shakmaty::File = posx.try_into().unwrap_or(shakmaty::File::A);
        let rank: shakmaty::Rank = posy.try_into().unwrap_or(shakmaty::Rank::First);

        let color;
        let colors = MarkArrow::colors();
        if modifiers.intersects(gtk::gdk::ModifierType::SHIFT_MASK) {
            color = Some(colors.1);
        } else if modifiers.intersects(gtk::gdk::ModifierType::CONTROL_MASK) {
            color = Some(colors.2);
        } else {
            color = None;
        }

        let new_mark = MarkArrow::new(file, rank, color);
        new_mark.set_visible(false);
        self.overlay.add_overlay(&new_mark);
        p.arrows.push(new_mark);
    }

    fn add_mark(&self, x: f64, y: f64, modifiers: gtk::gdk::ModifierType) {
        let mut p = self.private.borrow_mut();

        let board = self.grid.width() as f64;
        let posx = (x * 8.0 / board) as u8;
        let posy = ((board - y) * 8.0 / board) as u8;

        let file: shakmaty::File = posx.try_into().unwrap_or(shakmaty::File::A);
        let rank: shakmaty::Rank = posy.try_into().unwrap_or(shakmaty::Rank::First);

        let current = p.marks
                       .iter()
                       .enumerate()
                       .find(|(_i, m)| {
                            m.file() == file && m.rank() == rank
                        });

        if let Some((i, mark)) = current {
            self.overlay.remove_overlay(mark);
            p.marks.remove(i);
        } else {
            let color;
            let colors = MarkCircle::colors();
            if modifiers.intersects(gtk::gdk::ModifierType::SHIFT_MASK) {
                color = Some(colors.1);
            } else if modifiers.intersects(gtk::gdk::ModifierType::CONTROL_MASK) {
                color = Some(colors.2);
            } else {
                color = None;
            }

            let new_mark = MarkCircle::new(file, rank, color);
            self.overlay.add_overlay(&new_mark);
            p.marks.push(new_mark);
        }
    }

    fn update_sidebar(&self) {
        let board = self.clone();

        {
            let mut p = self.private.borrow_mut();
            if p.timeout_id.is_some() {
                let id = p.timeout_id.take().unwrap();
                id.remove();
            }
        }

        let s = glib::source::timeout_add_local(Duration::from_millis(500), move || {
            let state = board.private.borrow_mut().sidebar.update();
            match state {
                SidebarState::Timeout(c) => {
                    board.endgame(Some(!c), String::from("Timeout"));
                    board.private.borrow_mut().timeout_id.take();
                    glib::source::Continue(false)
                },
                _ => {
                    glib::source::Continue(true)
                },
            }
        });

        self.private.borrow_mut().timeout_id = Some(s);
    }

    fn reload_board_cells(&self) {
        let mut p = self.private.borrow_mut();
        let board = p.chess.board().clone();
        for column in p.board.iter_mut() {
            for cell in column.iter_mut() {
                cell.widget.remove_css_class("last-movement");
                cell.widget.remove_css_class("check");
                if let Some(piece) = board.piece_at(cell.square()) {
                    cell.set_piece(Some(&piece));
                } else {
                    cell.set_piece(None);
                }
            }
        }
    }

    fn create_row(index: u8) -> [Cell; 8] {
        [
            Cell::new(0, index),
            Cell::new(1, index),
            Cell::new(2, index),
            Cell::new(3, index),
            Cell::new(4, index),
            Cell::new(5, index),
            Cell::new(6, index),
            Cell::new(7, index),
        ]
    }

    fn add_cell_drop_target(&self) {
        let p = self.private.borrow();
        for column in p.board.iter() {
            for cell in column.iter() {
                let drop_target = gtk::DropTarget::new(glib::types::Type::STRING, gdk::DragAction::COPY);

                let to = cell.square();
                let pos = cell.text();
                let w = cell.widget.clone();
                let board = self.clone();
                drop_target.connect_drop(move |_target, value, _x, _y| {
                    if let Ok(source) = value.get::<String>() {
                        let parts: Vec<&str> = source.split('-').collect();
                        let from_cell = board.get_cell(parts[1]);
                        let from = from_cell.square();

                        if !board.is_legal(from, to) {
                            return false;
                        }

                        w.remove_css_class("check");
                        from_cell.set_check(false);

                        let text = format!("{source}-{pos}");
                        let _ = w.activate_action("win.move", Some(&text.to_variant()));

                        return true;
                    }

                    false
                });

                cell.widget.add_controller(&drop_target);
            }
        }
    }

    pub fn do_move(&mut self, movement: String) -> Result<(), &str> {
        let parts: Vec<&str> = movement.split("-").collect();

        let src_pos = parts[1];
        let dst_pos = parts[2];

        let from = self.get_cell(src_pos).square();
        let to = self.get_cell(dst_pos).square();

        // Real movement in chess engine
        if let Some(movement) = self.get_movement(from, to) {
            if movement.is_promotion() {
                let popover = gtk::Popover::new();
                let promotion_box = gtk::Box::new(gtk::Orientation::Horizontal, 6);

                let dst = self.get_cell(dst_pos);
                let color = {
                    let private = self.private.borrow();
                    let chess = private.chess.clone();
                    match chess.turn() {
                        Color::Black => "b",
                        Color::White => "w",
                    }
                };

                for p in ['Q', 'R', 'B', 'N'] {
                    let code = format!("{color}{p}");
                    let button = gtk::Button::with_label(&code);
                    promotion_box.append(&button);

                    button.style_context().add_class(&format!("piece-{code}"));
                    button.style_context().add_class(&format!("promotion-button"));

                    let board = self.clone();
                    let popover_clone = popover.clone();
                    let m = movement.clone();
                    button.connect_clicked(move |_button| {
                        let piece = Role::from_char(p);
                        let m = Move::Normal {
                            role: m.role(),
                            from: m.from().unwrap(),
                            to: m.to(),
                            capture: m.capture(),
                            promotion: piece,
                        };
                        board.play(m);
                        board.computer_play();
                        popover_clone.popdown();
                        popover_clone.unparent();
                    });
                }

                popover.set_parent(&dst.widget);
                popover.set_child(Some(&promotion_box));
                popover.popup();
            } else {
                self.play(movement);
            }
        }

        // TODO: handle single player
        self.computer_play();
        self.evaluate();

        Ok(())
    }

    fn evaluate(&self) {
        // TODO: show this evaluation in the interface :D
        if self.show_best_move {
            let future = {
                let p = self.private.borrow();
                p.engine.evaluate()
            };

            let turn = self.private.borrow().chess.turn();
            let analysis_bar = self.analysis_bar.clone();
            let ctx = glib::MainContext::default();
            ctx.spawn_local(async move {
                if let Ok(evaluation) = future.await {
                    // TODO: show the real evaluation number in the interface
                    let mut value = i32::abs(evaluation) as f64 / 100.0;
                    value = if value > 10.0 { 10.0 } else { value };

                    // value [-10, 10]
                    value = match (turn, evaluation > 0) {
                        (Color::White, true) => -value,
                        (Color::Black, true) => value,
                        (Color::White, false) => value,
                        (Color::Black, false) => -value,
                    };

                    // value [0, 20] -> [0, 100]
                    value = (value + 10.0) * 5.0;
                    println!("Evaluation {evaluation} -> {value}");
                    analysis_bar.set_value(value);
                }
            });
        }
    }

    fn play(&self, movement: Move) {
        // TODO: calculate from & to for Castle
        let from = movement.from().unwrap();
        let to = movement.to();
        let _ = self.place_pieces_after_move(&movement, &from, &to);
        self.set_last_movement(&from, &to);

        let mut private = self.private.borrow_mut();
        let chess = private.chess.clone();
        if let Ok(chess) = chess.play(&movement) {
            private.chess = chess.clone();
            private.current_pos = private.current_pos + 1;
            private.movements.push(chess.clone());
            private.sidebar.turn(&chess, &movement);
            drop(private);
            self.compute_history();
            self.engine_update();
            self.notify_events(&chess);
        }
    }

    /// Set special class to last movement ceels
    fn set_last_movement(&self, from: &Square, to: &Square) {
        let p = self.private.borrow();
        for column in p.board.iter() {
            for cell in column.iter() {
                cell.widget.remove_css_class("last-movement");
                let sq = cell.square();
                if sq == *from || sq == *to {
                    cell.widget.add_css_class("last-movement");
                }
            }
        }
    }

    fn place_pieces_after_move(&self, movement: &Move, from: &Square, to: &Square)
        -> Result<(), &str>
    {
        let src_cell = self.cell_from_square(*from);
        let dst_cell = self.cell_from_square(*to);
        let piece = src_cell.piece().ok_or("")?;
        src_cell.remove_piece();
        dst_cell.set_piece(Some(&piece));

        src_cell.set_check(false);
        dst_cell.set_check(false);

        // En Passant
        if let Move::EnPassant { .. } = movement {
            let f = to.file();
            let r = from.rank();
            let capture = Square::from_coords(f, r);
            let capture_cell = self.cell_from_square(capture);
            capture_cell.set_piece(None);
        };

        // Promotion
        if let Move::Normal { promotion: Some(r), .. } = movement {
            let p = Piece { color: piece.color, role: *r };
            dst_cell.set_piece(Some(&p));
        }

        if let Move::Castle { king: _, rook: sq } = movement {
            // Move rook in castle
            let rook_cell = self.cell_from_square(*sq);
            rook_cell.remove_piece();
            let (king, rook) = match sq {
                Square::A1 => {
                    (self.cell_from_square(Square::C1), self.cell_from_square(Square::D1))
                },
                Square::H1 => {
                    (self.cell_from_square(Square::G1), self.cell_from_square(Square::F1))
                },
                Square::A8 => {
                    (self.cell_from_square(Square::C8), self.cell_from_square(Square::D8))
                },
                Square::H8 => {
                    (self.cell_from_square(Square::G8), self.cell_from_square(Square::F8))
                },
                _ => panic!("Unreacheable!"),
            };
            rook.set_piece(Some(&piece.color.rook()));
            king.set_piece(Some(&piece.color.king()));
        }

        Ok(())
    }

    fn notify_events<T>(&self, position: &T) where T: Position {
        if let Some(square) = position.board().king_of(position.turn()) {
            let cell = self.cell_from_square(square);
            cell.set_check(position.is_check());
        }
        if let Some(square) = position.board().king_of(!position.turn()) {
            let cell = self.cell_from_square(square);
            cell.set_check(false);
        }

        let (text, color) = if position.is_checkmate() {
            (String::from("Checkmate"), Some(!position.turn()))
        } else if position.is_stalemate() {
            (format!("{} can't move", position.turn()), None)
        } else if position.is_insufficient_material() {
            (String::from("Draw! Insufficient material to win"), None)
        } else {
            return;
        };

        self.endgame(color, text);
    }

    fn endgame(&self, winner: Option<Color>, msg: String) {
        {
            let mut p = self.private.borrow_mut();
            p.canmove = false;
            p.sidebar.stopclocks();
        }

        let text = match winner {
            Some(c) => format!("{} wins!", c.to_string()),
            None => format!("Draw"),
        };

        let text = format!("{}: {}", text, msg);
        let _ = self.board_widget.activate_action("win.toast", Some(&text.to_variant()));
    }

    pub fn cell_from_square(&self, square: Square) -> Cell {
        let private = self.private.borrow();
        let c: usize = square.file().try_into().unwrap();
        let r: usize = square.rank().try_into().unwrap();
        private.board[r][c].clone()
    }

    pub fn get_cell(&self, strpos: &str) -> Cell {
        let c: usize = match strpos.chars().next() {
            Some('a') => 0,
            Some('b') => 1,
            Some('c') => 2,
            Some('d') => 3,
            Some('e') => 4,
            Some('f') => 5,
            Some('g') => 6,
            Some('h') => 7,
            _ => 0,
        };

        let r = usize::from_str_radix(&strpos[1..], 10).unwrap_or(1) - 1;

        let private = self.private.borrow();
        private.board[r][c].clone()
    }

    pub fn is_legal(&self, from: Square, to: Square) -> bool {
        let private = self.private.borrow();
        if !private.canmove {
            return false;
        }

        match self.get_movement(from, to) {
            Some(movement) => private.chess.is_legal(&movement),
            None => false,
        }
    }

    pub fn get_movement(&self, from: Square, to: Square) -> Option<Move> {
        let private = self.private.borrow();
        let role = private.chess.board().role_at(from)?;
        let capture = private.chess.board().role_at(to);

        // Check if the movement is a castle
        // White king: e1 -> g1, e1 -> c1
        // Black king: e8 -> g8, e8 -> c8
        if role == Role::King {
            let castle = match (from, to) {
                (Square::E1, Square::G1) | (Square::E1, Square::H1) => {
                    Some(Move::Castle { king: Square::E1, rook: Square::H1 })
                },
                (Square::E1, Square::C1) | (Square::E1, Square::A1) => {
                    Some(Move::Castle { king: Square::E1, rook: Square::A1 })
                },
                (Square::E8, Square::G8) | (Square::E8, Square::H8) => {
                    Some(Move::Castle { king: Square::E8, rook: Square::H8 })
                },
                (Square::E8, Square::C8) | (Square::E8, Square::A8) => {
                    Some(Move::Castle { king: Square::E8, rook: Square::A8 })
                },
                _ => None,
            };
            if castle.is_some() {
                return castle;
            }
        }

        let moves = private.chess.legal_moves();
        let mut promotions = moves.iter().filter(|m| m.is_promotion());
        let mut passant = moves.iter().filter(|m| m.is_en_passant());

        // En passant
        if let Some(m) = passant.find(|m| m.from() == Some(from) && m.to() == to) {
            return Some(m.clone());
        }

        let mut promotion = None;
        if let Some(m) = promotions.find(|m| m.from() == Some(from) && m.to() == to) {
            promotion = m.promotion();
        }

        let movement = Move::Normal {
            role,
            capture,
            from,
            to,
            promotion,
        };

        Some(movement)
    }

    pub fn load_fen(&mut self, contents: String) -> std::io::Result<Chess> {
        let error = Err(std::io::Error::new(std::io::ErrorKind::Other, "oh no! Loading FEN"));
        let fen = Fen::from_ascii(contents.trim().as_bytes()).or(error)?;
        let error = Err(std::io::Error::new(std::io::ErrorKind::Other, "oh no! Into position"));
        let pos = fen.into_position::<Chess>(CastlingMode::Standard).or(error)?;

        Ok(pos)
    }

    pub fn load_pgn(&mut self, contents: String) -> std::io::Result<Vec<Chess>> {
        let error = Err(std::io::Error::new(std::io::ErrorKind::Other, "oh no! Loading PGN"));
        let movements = PGN::from_bytes(contents.as_bytes()).or(error)?;

        Ok(movements)
    }

    /// Load a FEN or PGN state from a file
    pub fn load(&mut self, path: &str, file_type: FileType) -> std::io::Result<()> {
        let mut file = File::open(path)?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;

        let pos = match file_type {
            FileType::FEN => vec![self.load_fen(contents)?],
            FileType::PGN => self.load_pgn(contents)?,
        };

        self.set_analysis(pos);

        Ok(())
    }

    pub fn set_analysis(&mut self, movements: Vec<Chess>) {
        {
            let mut private = self.private.borrow_mut();
            private.chess = match movements.len() {
                0 => Chess::default(),
                _ => movements[0].clone(),
            };
            private.movements = movements;
            private.current_pos = 0;
            private.canmove = true;
            private.sidebar.set_time(None);
            // TODO: set analysis mode instead of skill level
            private.engine.set_level(SkillLevel::Eight)
                .expect("Error setting engine skill level");
            self.analysis_bar.set_visible(true);
        }

        self.engine_update();
        self.reload_board_cells();
        self.compute_history();

        {
            let mut private = self.private.borrow_mut();
            private.sidebar.clear();
        }

        self.computer = None;
        self.show_best_move = true;
    }

    pub fn analysis(&mut self) {
        self.set_analysis(vec![Chess::default()]);
    }

    fn initialize_cells(&self) {
        let p = self.private.borrow();
        for column in p.board.iter() {
            for cell in column.iter() {
                let r = 7 - cell.row() as i32;
                let c = cell.column() as i32;
                self.grid.attach(&cell.widget, c, r, 1, 1);
            }
        }
    }

    pub fn rotate(&mut self) {
        let mut p = self.private.borrow_mut();
        p.sidebar.rotate();
        self.rotated = !self.rotated;
        if self.rotated {
            self.analysis_bar.style_context().add_class("rotated");
            self.board_widget.style_context().add_class("rotated");
        } else {
            self.analysis_bar.style_context().remove_class("rotated");
            self.board_widget.style_context().remove_class("rotated");
        }
    }

    pub fn new_game(&mut self, color: Color, time: u32, level: SkillLevel) {
        {
            let mut p = self.private.borrow_mut();
            p.chess = Chess::default();
            p.movements = vec![p.chess.clone()];
            p.current_pos = 0;
            p.sidebar.clear();
            p.sidebar.set_time(Some(time as u64 * 60));
            p.engine.set_level(level).expect("Error setting engine skill level");
            p.canmove = true;
            self.analysis_bar.set_visible(false);
        }
        self.computer = Some(!color);
        self.show_best_move = false;

        if color == Color::Black && !self.rotated {
            self.rotate();
        } else if color == Color::White && self.rotated {
            self.rotate();
        }

        self.engine_update();
        self.reload_board_cells();
        self.update_sidebar();
        self.computer_play();
        self.compute_history();
    }

    fn engine_update(&self) {
        let mut p = self.private.borrow_mut();
        let c = p.chess.clone();
        p.engine.update(c);
    }

    /// move using the engine
    fn computer_play(&self) {
        let future = {
            let p = self.private.borrow();
            if !p.canmove {
                return;
            }

            match self.computer {
                Some(c) if c == p.chess.turn() => p.engine.calculate_move(),
                None if self.show_best_move => p.engine.calculate_move(),
                _ => return,
            }
        };

        let board = self.clone();
        let ctx = glib::MainContext::default();
        ctx.spawn_local(async move {
            if let Ok(next_move) = future.await {
                if board.show_best_move {
                    board.show_move_arrow(next_move);
                } else {
                    board.play(next_move);
                }
            }
        });
    }

    fn compute_history(&self) {
        let p = self.private.borrow();
        let (prev, next) = (p.current_pos > 0, p.current_pos < p.movements.len() - 1);
        if let Some(w) = self.main_window() {
            w.action_set_enabled("win.go-prev", prev);
            w.action_set_enabled("win.go-next", next);
        }
    }

    pub fn go(&self, to: History) {
        {
            let mut p = self.private.borrow_mut();
            p.current_pos = match to {
                History::Next if p.current_pos < p.movements.len() - 1 => p.current_pos + 1,
                History::Prev if p.current_pos > 0 => p.current_pos - 1,
                _ => p.current_pos,
            };
            p.chess = p.movements[p.current_pos].clone();
        };
        self.reload_board_cells();
        self.compute_history();
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let private = self.private.borrow();
        for column in private.board.iter() {
            for row in column.iter() {
                let _ = write!(f, "({}, {}) {}, ", row.column_name(), row.row_name(), row.color());
            }
            let _ = write!(f, "\n");
        }
        Ok(())
    }
}

