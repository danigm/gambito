use std::rc::Rc;
use std::cell::RefCell;
use std::convert::TryFrom;

use gtk;
use gtk::prelude::*;
use gtk::glib;
use gtk::gdk;
use shakmaty::{Square, File, Rank, Piece, Color};

use crate::piece::GambitoPiece;

#[derive(Clone)]
#[derive(Debug)]
struct CellPrivate {
    // [column, row]
    pub pos: [u8; 2],
    pub piece: Option<Piece>,
}

#[derive(Clone)]
#[derive(Debug)]
pub struct Cell {
    pub widget: gtk::Button,
    pub label: gtk::Label,
    private: Rc<RefCell<CellPrivate>>,
}

impl Default for Cell {
    fn default() -> Self {
        Self::new(0, 0)
    }
}

impl Cell {
    pub fn new(column: u8, row: u8) -> Self {
        let widget = gtk::Button::new();
        widget.set_hexpand(true);
        widget.set_vexpand(true);

        let label = gtk::Label::new(None);
        label.set_hexpand(true);
        label.set_xalign(0.5);
        widget.set_child(Some(&label));

        let private = CellPrivate { pos: [column, row], piece: None };
        let object = Self {
            widget,
            label,
            private: Rc::new(RefCell::new(private)),
        };

        object.update_label();
        object.add_drag_source();
        object.widget.add_css_class("board-cell");
        match object.color() {
            Color::Black => object.widget.add_css_class("black"),
            Color::White => object.widget.add_css_class("white"),
        };

        object
    }

    fn add_drag_source(&self)  {
        let drag_source = gtk::DragSource::new();

        let widget = self.label.clone();
        let private = self.private.clone();
        let pos = self.text();
        drag_source.connect_prepare(move |source, x, y| {
            let p = private.borrow();
            match p.piece {
                Some(ref piece) => {
                    let piece_name = piece.get_text();
                    let text = format!("{piece_name}-{pos}");
                    let bytes = glib::Bytes::from(&text.as_ref());
                    let provider = gdk::ContentProvider::for_bytes("text/plain", &bytes);

                    // Attach the piece image to the drag position
                    let paintable = gtk::WidgetPaintable::new(Some(&widget));
                    source.set_icon(Some(&paintable), x as i32, y as i32);

                    Some(provider)
                }
                None => None,
            }
        });

        let w = self.widget.clone();
        drag_source.connect_drag_begin(move |_source, _drag| {
            w.add_css_class("drag");
        });

        let w = self.widget.clone();
        drag_source.connect_drag_end(move |_source, _drag, _moved| {
            w.remove_css_class("drag");
        });

        self.label.add_controller(&drag_source);
    }

    pub fn text(&self) -> String {
        format!("{}{}", self.column_name(), self.row_name())
    }

    pub fn update_label(&self) {
        self.label.set_text(&self.text());
    }

    pub fn column(&self) -> u8 {
        let private = self.private.borrow();
        private.pos[0]
    }

    pub fn column_name(&self) -> char {
        let private = self.private.borrow();
        let names = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' ];
        names[private.pos[0] as usize]
    }

    pub fn row(&self) -> u8 {
        let private = self.private.borrow();
        private.pos[1]
    }

    pub fn row_name(&self) -> char {
        let private = self.private.borrow();
        let names = [ '1', '2', '3', '4', '5', '6', '7', '8' ];
        names[private.pos[1] as usize]
    }

    pub fn color(&self) -> Color {
        let private = self.private.borrow();
        let row_i = private.pos[1] % 2 == 0;
        let column_i = private.pos[0] % 2 == 0;
        if row_i {
            if column_i { Color::Black } else { Color::White }
        } else {
            if column_i { Color::White } else { Color::Black }
        }
    }

    pub fn set_piece(&self, piece: Option<&Piece>) {
        let mut private = self.private.borrow_mut();
        let context = self.widget.style_context();

        if let Some(ref piece) = private.piece {
            context.remove_class(&piece.get_class());
        }

        match piece {
            None => private.piece = None,
            Some(ref p) => {
                context.add_class(&p.get_class());
                private.piece = Some(*p.clone());
            },
        };
    }

    pub fn remove_piece(&self) {
        self.set_piece(None);
    }

    pub fn pos(&self) -> (u8, u8) {
        let private = self.private.borrow();
        (private.pos[0], private.pos[1])
    }

    pub fn square(&self) -> Square {
        let p = self.pos();
        let file = File::try_from(p.0).unwrap_or(File::A);
        let rank = Rank::try_from(p.1).unwrap_or(Rank::First);
        Square::from_coords(file, rank)
    }

    pub fn piece(&self) -> Option<Piece> {
        let private = self.private.borrow();
        match private.piece {
            None => None,
            Some(ref p) => Some(p.clone()),
        }
    }

    pub fn set_check(&self, check: bool) {
        if check {
            self.widget.add_css_class("check");
        } else {
            self.widget.remove_css_class("check");
        }
    }
}
