use gettextrs::*;
use gtk::prelude::*;
use gtk::gio;
use gtk::gdk;
use adw;

mod config;
mod window;
mod board;
mod cell;
mod piece;
mod sidebar;
mod chessengine;
use crate::window::Window;

mod mark_circle;
mod mark_arrow;

mod pgn;

fn main() {
    let application = adw::Application::new(
        Some("net.danigm.gambito"),
        Default::default(),
    );

    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain("gambito", config::LOCALEDIR);
    textdomain("gambito");

    application.connect_activate(build_ui);
    application.run();
}

fn build_ui(application: &adw::Application) {
    let res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/gambito.gresource")
        .expect("Could not load resources");
    gio::resources_register(&res);

    let provider = gtk::CssProvider::new();
    provider.load_from_resource("/net/danigm/gambito/gambito.css");
    gtk::StyleContext::add_provider_for_display(
        &gdk::Display::default().expect("Error initializing gtk css provider."),
        &provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );

    let window = Window::new();

    window.widget.set_application(Some(application));
    window.widget.show();
}
