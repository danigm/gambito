# Gambito

A Gtk4 chess application. The main functionalities are:

 * Play chess against stockfish
 * Open a FEN file to analyse
 * Open a PGN file to analyse
 * Draw marks and arrows with right click

![screenshot](https://gitlab.gnome.org/danigm/gambito/raw/main/screenshots/gambito-screenshot01.png)
![screenshot](https://gitlab.gnome.org/danigm/gambito/raw/main/screenshots/gambito-screenshot02.png)
![screenshot](https://gitlab.gnome.org/danigm/gambito/raw/main/screenshots/gambito-screenshot03.png)
